﻿using LibraryManagementCourse.Data.Model;

namespace LibraryManagementCourse.ViewsModel
{
    public class CustomerViewModel
    {
        public Customer Customer { get; set; }
        public int BookCount { get; set; }
    }

}
